# Jekyll webcomic generator

A simple webcomic template for Jekyll to generate comic pages which supports multiple comics on one site.
The ruby script is what I use to generate [yncke.be](https://www.yncke.be). The rest of the files result in a ~~ugly~~ retro site and are merely meant as an illustration of how it can be used. Writing sensible HTML and CSS is not part of this template.

## Features

The generater creates pages with the template 'comicpage.html' based on the yml files that are set in the 'config.yml'.
The een\_stripje.yml shows the minimum needed to generate a comic, some\_comic.yml contains all the bells and whistles that are currently supported. These are a good starting point to use as an example.

A generated page contains the following liquid tags:
- **series** The title of the comic series. As defined by *series\_title*.
- **chapter** The title of the chapter. As defined by *chapter\_title*.
- **language** A string describing the language . As defined by *language*.
- **img_url** The relative path to the image. As defined by *img\_url*.
- **img_alt** The alt text for the image. As defined by *img\_alt*, or if absent, generated based on comic series, chapter and page.
- **title** The title of the page. As defined by *title*.
- **description** A short description. As defined by *description*
- **long\_description** A long description. As defined by *long_description*.
- **permalink** The full URL of the page. This is generated as "*site.baseurl* / *output_rootfolder* / *series_id*-*chapter\_id* / *page_index*.html". If *series_id* and/or *chapter\_id* are not defined, they are omitted.
- **output_rootfolder** The root folder for the generated files. By default, the files are generated in "html".
- **comments** Author notes. As defined by *comments*.
- **index** The index of the current page. One based. If there's a cover, it has page number 0.
- **total\_nb\_pages** The total number of pages for this chapter.
- **previous_in_chapter** The URL of the previous page.
- **next\_in\_chapter** The URL of the next page.
- **first\_in\_chapter** The URL of the first page of the chapter.
- **last\_in\_chapter** The URL of the last page of the chapter.
- **keywords** Keywords for this page. They are a combination of the page and chapter keywords.
- **next\_comic\_first\_page** The URL of the first page of the next comic or chapter, if defined.
- **next\_comic\_last\_page** The URL of the last page of the next comic or chapter, if defined.
- **previous\_comic\_first\_page** The URL of the first page of the next comic or chapter, if defined.
- **previous\_comic\_last\_page** The URL of the last page of the next comic or chapter, if defined.


## Background

I had an old website of hand coded HTML pages which I wanted to update. After some searching, Jekyll looked like what I needed. After some more searching, I came across the [comical jekyll theme](https://github.com/chrisanthropic/comical-jekyll-theme) which was very eductional, but didn't fit my needs because I have more than one comic on the same site.

So I constructed a plugin based on a [datapage generator](https://github.com/avillafiorita/jekyll-datapage_gen) for this, which you can find here.

## Disclaimer

I'm not a ruby developer. This project has been put online in the hope that it can be useful to someone. Please don't hold me responsible if it wastes your time, eats your cat, blows up your dinner or doesn't work at all.