# coding: utf-8
# Generate comic pages from individual records in yml files
# Copy pasted parts from data_page_generator from https://github.com/avillafiorita/jekyll-datapage_gen
# by Adolfo Villafiorita, which was distributed under the conditions of the MIT License, so I guess this
# is MIT License too.

def generateFileName(page_index)
   return page_index.to_s + ".html"
end # def

def getPageUrl(comic_chapter, page_index)
   series_id = comic_chapter['series_id']
   chapter_id = comic_chapter['chapter_id']
   if comic_chapter['output_rootfolder']
       file_path = "/" + comic_chapter['output_rootfolder'] + "/"
   else
       file_path = "/html/"
   end
   if series_id
       file_path += series_id
       if chapter_id && chapter_id != ""
           file_path += "-"
       else
           file_path += "/"
       end # if
   end # if
   if chapter_id && chapter_id != ""
       file_path += chapter_id.to_s + "/"
   end # if
   file_path += generateFileName(page_index)
end # def

# This allows to use the series + chapter combination as a hash
class ComicChapter
    attr_reader :series_id, :chapter_id

    def initialize(series_id, chapter_id)
        @series_id = series_id
        if chapter_id
            @chapter_id = chapter_id
        else
            @chapter_id = ""
        end # if
    end

    def ==(other)
        self.class === other and
        other.series_id == @series_id and
        other.chapter_id == @chapter_id
    end

    alias eql? ==

    def hash
        @series_id.hash ^ @chapter_id.hash
    end
end

module Jekyll

    # this class is used to tell Jekyll to generate a page
    class ComicPage < Page

     # - site and base are copied from other plugins: to be honest, I am not sure what they do
     def initialize(site, base, comic_chapter, comic_page, index)
        @site = site
        @base = base
        @comic_chapter = comic_chapter
        @comic_page = comic_page

        calculateIndices(index)

        self.process(generateFileName(@pageindex))
        self.read_yaml(File.join(base, '_layouts'), "comicpage.html")

        calculatePageNavigators()
        appendMetadata()

        # add all the information defined in _data for the current record to the
        # current page (so that we can access it with liquid tags)
        self.data.merge!(data)
     end # def

     def validateSettings()
         if @comic_chapter['series_title'].isNil()
             error "No series name specified"
         end
         if @comic_chapter['comic_pages'].length == 0
             error "No comic pages specified for " + @comic_chapter['series_title']
         end
     end # def

     def calculateIndices(index)
        maxindex = @comic_chapter['comic_pages'].length
        @lastindex = maxindex
        @pageindex = index + 1
        @firstindex = 1
        @pagemaxindex = maxindex
        if @comic_chapter['hascover']
            @pageindex = index
            @firstindex = 0
            @pagemaxindex = maxindex - 1
        end
     end # def

     def appendMetadata()
         self.data['series'] = @comic_chapter['series_title']
         self.data['chapter'] = @comic_chapter['chapter_title']
         self.data['language'] = @comic_chapter['language']
         self.data['img_url'] = @comic_page['img_url']
         self.data['image'] = @comic_page['img_url']
         if @comic_page['alt']
            self.data['img_alt'] = @comic_page['img_alt']
         else
              self.data['img_alt'] = @comic_chapter['series_title'] + " p. " + @pageindex.to_s
         end
         keywords = @comic_page['keywords']
         if @comic_chapter['keywords']
             if @comic_page['keywords']
                 keywords +=  ", " + @comic_chapter['keywords']
             else
                 keywords = @comic_chapter['keywords']
             end
         end
         self.data['keywords'] = keywords
         self.data['description'] = @comic_page['description']
         self.data['comments'] = @comic_page['comments']
         self.data['long_description'] = @comic_page['long_description']
           self.data['permalink'] = calculatePageUrl(@pageindex)
           self.data['index'] = @pageindex
           self.data['total_nb_pages'] = @pagemaxindex
         if @comic_page['date']
             self.data['date'] = @comic_page['date']
         else
             self.data['date'] = Time.new(0)
         end
         ensureTitle()
     end # def

     def calculatePageUrl(page_index)
        file_path = getPageUrl(@comic_chapter, page_index)
     end # def

     def ensureTitle()
        if @comic_page['title']
            self.data['title'] = @comic_page['title']
        else
            if @comic_chapter['title_template']
                title = @comic_chapter['title_template'].clone
                strings_to_replace = title.scan(/{{([a-z_A-Z0-9]+)}}/)
                strings_to_replace.each do |string_to_replace|
                    key = string_to_replace.first
                    closed_string = "{{" + key.to_s + "}}"
                    title.sub! closed_string, self.data[key].to_s
                 end # do
            else
                title = @comic_chapter['series_title']
                if @comic_chapter['chapter_title']
                    title += " " + @comic_chapter['chapter_title']
                end #if
            end # if
            self.data['title'] = title
        end # if
     end # def

     def calculatePageNavigators()
        if @pageindex > @firstindex
            self.data['first_in_chapter'] = calculatePageUrl(@firstindex)
            previousindex = @pageindex - 1
            self.data['previous_in_chapter'] = calculatePageUrl(previousindex)
        end # if
        if @pageindex < @pagemaxindex
            nextindex = @pageindex + 1
            self.data['next_in_chapter'] = calculatePageUrl(nextindex)
        end # if
        self.data['last_in_chapter'] = calculatePageUrl(@pagemaxindex)
     end # def
   end # class

   class ComicPagesGenerator < Generator
     safe true

     def calculateComicNavigatorUrls()
        @comic_chapter_first_page_urls = {}
        @comic_chapter_last_page_urls = {}
        @comics.each do |comic|
            yml_file = comic['config_file']
            yml_data = @site.data[yml_file]
            if yml_data
                yml_data.each do |comic_chapter|
                    if comic_chapter['hascover']
                        first_page_url = getPageUrl(comic_chapter, 0)
                        last_page_url = getPageUrl(comic_chapter, comic_chapter['comic_pages'].length - 1)
                    else
                        first_page_url = getPageUrl(comic_chapter, 1)
                        last_page_url = getPageUrl(comic_chapter, comic_chapter['comic_pages'].length)
                    end
                    @comic_chapter_first_page_urls[ComicChapter.new(comic_chapter['series_id'], comic_chapter['chapter_id'])] = first_page_url
                    @comic_chapter_last_page_urls[ComicChapter.new(comic_chapter['series_id'], comic_chapter['chapter_id'])] = last_page_url
                end
            end
        end # comics.each
     end

     def calculateComicNavigators(comic_chapter, generated_page)
        if comic_chapter['next_comic']
            comic_chapter_hash =  ComicChapter.new(comic_chapter['next_comic']['series_id'], comic_chapter['next_comic']['chapter_id'])
            generated_page.data['next_comic_first_page'] = @comic_chapter_first_page_urls[comic_chapter_hash]
            generated_page.data['next_comic_last_page'] = @comic_chapter_last_page_urls[comic_chapter_hash]
        end
        if comic_chapter['previous_comic']
            comic_chapter_hash =  ComicChapter.new(comic_chapter['previous_comic']['series_id'], comic_chapter['previous_comic']['chapter_id'])
            generated_page.data['previous_comic_first_page'] = @comic_chapter_first_page_urls[comic_chapter_hash]
            generated_page.data['previous_comic_last_page'] = @comic_chapter_last_page_urls[comic_chapter_hash]
        end

     end

     def addToCollection(comic_chapter, generated_page)
        collection_to_add_to = "comicpages"
        if comic_chapter['collection']
            collection_to_add_to = comic_chapter['collection']
        end
        if @site.collections[collection_to_add_to]
            @site.collections[collection_to_add_to].docs << generated_page
        end
     end

     def generateComicPages()
         @comics.each do |comic|
             # The matching yml file for the comic pages
             yml_file = comic['config_file']
             yml_data = @site.data[yml_file]
             if yml_data
                 yml_data.each do |comic_chapter|
                    Jekyll.logger.info "Webcomic generator:", "Generating comic pages for " + comic_chapter['series_title'] + " " + comic_chapter['chapter_title'].to_s
                    comic_pages = comic_chapter['comic_pages']
                    comic_pages.each do |comic_page|
                        index = comic_pages.index comic_page
                        generated_page = ComicPage.new(@site, @site.source, comic_chapter, comic_page, index)
                        calculateComicNavigators(comic_chapter, generated_page)
                        @site.pages << generated_page
                        addToCollection(comic_chapter, generated_page)
                    end # comic_pages.each
                end # yml_data.each
             else
                 warn "The yml file " + yml_file + " does not exist."
             end
          end # comics.each
     end

     def generate(site)
         @site = site
         @comics = site.config['comics_to_generate']
         if @comics
             calculateComicNavigatorUrls()
             generateComicPages()
            # The list of comic pages
         end # if comics
      end # generate(site)
    end # class ComicPagesGenerator

end # module

